from plone.app.testing import PloneWithPackageLayer
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting

import csc.theme


CSC_THEME = PloneWithPackageLayer(
    zcml_package=csc.theme,
    zcml_filename='testing.zcml',
    gs_profile_id='csc.theme:testing',
    name="CSC_THEME")

CSC_THEME_INTEGRATION = IntegrationTesting(
    bases=(CSC_THEME, ),
    name="CSC_THEME_INTEGRATION")

CSC_THEME_FUNCTIONAL = FunctionalTesting(
    bases=(CSC_THEME, ),
    name="CSC_THEME_FUNCTIONAL")
